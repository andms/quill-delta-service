const express = require('express');
const Delta = require('quill-delta');
const QuillDeltaToHtmlConverter = require('quill-delta-to-html').QuillDeltaToHtmlConverter;

const router = express.Router();

router.post('/delta/merge', async (req, res) => {
    
    if(req.body.oldDelta && req.body.newDelta) {
        const oldDeltaParam = req.body.oldDelta;
        const oldDelta = new Delta(oldDeltaParam);
    
        const newDeltaParam = req.body.newDelta;
        const newDelta = new Delta(newDeltaParam);
    
        const updatedDelta = oldDelta.compose(newDelta);
        res.status(200).send(updatedDelta);
    }
    else if(req.body.oldDelta) {
        res.status(200).send(req.body.old);
    }
    else {
        res.status(200).send(req.body.new);
    }
});

router.post('/delta/tohtml', async (req, res) => {
    
    if(req.body && req.body.ops) {
        const deltaOps = req.body.ops;
        const cfg = {};
        const converter = new QuillDeltaToHtmlConverter(deltaOps, cfg);
        const html = converter.convert(); 
        res.status(200).send(html);
    }
    else {
        res.status(400).send("Bad request sent to delta html converter");
    }
});

module.exports = router;